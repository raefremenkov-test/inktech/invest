<?php

use Illuminate\Support\Facades\Route;

// Получить ифнормацию обо всех сервисах, можно добавить пагинацию (в контроллере)
Route::get('/exchanges', [\App\Ui\Adapters\Http\Controllers\ExchangeController::class, 'getAll']);

// Получить ифнормацию о конкретном сервисе с актуальной информацией
Route::get('/exchanges/{exchange_id}', [\App\Ui\Adapters\Http\Controllers\ExchangeController::class, 'get']);
