<?php

namespace App\Core\Application\UseCases\Queries\GetExchange;

use App\Core\Ports\IExchangeRepository;

class Query
{
    public function __construct(
        protected IExchangeRepository $exchangeRepository
    ) {
        //
    }

    public function execute(string $exchangeId): Response
    {
        $exchange = $this->exchangeRepository->get($exchangeId);

        return new Response(
            $exchange->id,
            $exchange->name,
            $exchange->url,
            $exchange->information
        );
    }
}
