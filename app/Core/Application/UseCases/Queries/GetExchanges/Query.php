<?php

namespace App\Core\Application\UseCases\Queries\GetExchanges;

use App\Core\Ports\IExchangeRepository;

class Query
{
    public function __construct(
        protected IExchangeRepository $exchangeRepository
    ) {
        //
    }

    public function execute(): Response
    {
        $exchanges = $this->exchangeRepository->getAll();

        // Мапим доменные сущности сервисов на DTO
        $mappedExchanges = [];

        return new Response($mappedExchanges);
    }
}
