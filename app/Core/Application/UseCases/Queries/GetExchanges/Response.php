<?php

namespace App\Core\Application\UseCases\Queries\GetExchanges;

/**
 * @property-read array $exchanges
 */
class Response
{
    public function __construct(
        protected array $exchanges
    ) {
        //
    }

    public function __get(string $name)
    {
        return $this->$name;
    }
}
