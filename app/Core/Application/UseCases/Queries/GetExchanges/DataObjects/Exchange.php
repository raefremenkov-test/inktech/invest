<?php

namespace App\Core\Application\UseCases\Queries\GetExchanges\DataObjects;

/**
 * @property-read string $exchangeId
 * @property-read string $exchangeName
 * @property-read string $exchangeUrl
 * @property-read array $exchangeInformation
 */
class Exchange
{
    public function __construct(
        protected string $exchangeId,
        protected string $exchangeName,
        protected string $exchangeUrl,
        protected array $exchangeInformation,
    ) {
        //
    }

    public function __get(string $name)
    {
        return $this->$name;
    }
}
