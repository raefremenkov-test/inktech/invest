<?php

namespace App\Core\Application\UseCases\Commands\UpdateExchangeInformation;

use App\Core\Ports\IExchangeRepository;
use App\Core\Ports\IExchangeService;

class Command
{
    public function __construct(
        protected IExchangeService    $exchangeService,
        protected IExchangeRepository $exchangeRepository
    ) {
        //
    }

    public function handle(string $exchangeId, $exchangeUrl): void
    {
        $info = $this->exchangeService->getInformation($exchangeId, $exchangeUrl);

        $exchange = $this->exchangeRepository->get($exchangeId);

        $exchange->updateInformation($info);

        $this->exchangeRepository->update($exchange);
    }
}
