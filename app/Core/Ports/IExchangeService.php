<?php

namespace App\Core\Ports;

use App\Core\Domain\ExchangeAggregate\Exchange;

/**
 * Используем DI
 */
interface IExchangeService
{
    public function getInformation(string $exchangeId, string $exchangeUrl): array;
}
