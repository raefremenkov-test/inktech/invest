<?php

namespace App\Core\Ports;

use App\Core\Domain\ExchangeAggregate\Exchange;

/**
 * Используем DI
 */
interface IExchangeRepository
{
    public function get(string $exchangeId): Exchange;
    public function update(Exchange $exchange): Exchange;
    public function getAll(): array;
}
