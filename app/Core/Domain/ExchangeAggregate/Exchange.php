<?php

namespace App\Core\Domain\ExchangeAggregate;

/**
 * @property-read string $id
 * @property-read string $name
 * @property-read string $url
 * @property-read string $information
 */
class Exchange
{
    protected array $information = [];

    public function __construct(
        protected string $id,
        protected string $name,
        protected string $url,
    ) {
        //
    }

    public function __get(string $name)
    {
        return $this->$name;
    }

    public function updateInformation(array $information): void
    {
        $this->information = $information;
    }
}
