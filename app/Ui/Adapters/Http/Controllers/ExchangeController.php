<?php

namespace App\Ui\Adapters\Http\Controllers;

use App\Core\Application\UseCases\Queries\GetExchanges\Query;
use \App\Core\Application\UseCases\Queries\GetExchange\Query as GetExchangeQuery;

class ExchangeController extends Controller
{
    public function __construct(
        public Query $getExchangesQuery,
        public GetExchangeQuery $getExchangeQuery
    ) {
        //
    }

    public function getAll()
    {
        $queryResponse = $this->getExchangesQuery->execute();

        // получаем id сервисов из $queryResponse
        $exchangesIds = [];

        return response()
            ->json(['ids' => $exchangesIds]);
    }

    public function get(string $exchangeId)
    {
        $data = $this->getExchangeQuery->execute($exchangeId);

        return response()
            ->json(['data' => $data]);


    }
}
