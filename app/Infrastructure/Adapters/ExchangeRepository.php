<?php

namespace App\Infrastructure\Adapters;

use App\Core\Domain\ExchangeAggregate\Exchange;
use App\Core\Ports\IExchangeRepository;

/**
 * Используем DI
 */
class ExchangeRepository implements IExchangeRepository
{

    public function get(string $exchangeId): Exchange
    {
        // TODO: Implement get() method.
    }

    public function update(Exchange $exchange): Exchange
    {
        // TODO: Implement update() method.
    }

    public function getAll(): array
    {
        // TODO: Implement getAll() method.
    }
}
