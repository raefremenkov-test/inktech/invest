<?php

namespace App\Infrastructure\BackgroundJobs;
use App\Core\Application\UseCases\Commands\UpdateExchangeInformation\Command;
use App\Core\Application\UseCases\Queries\GetExchanges\Query;

class ActualizeExchangesInformationJob implements ShouldQueue
{
    public function __construct(
        protected Query $query,
        protected Command $command
    ) {
    }

    /**
     * Execute the job.
     * Джобу запускаем по крону. Частотность зависит от конкретных бизнес задач, от того насколько необходимо иметь актуальные данные и от самих сервисов.
     */
    public function handle()
    {
        $exchanges = $this->query->execute(); // для списка exchanges можно использовать генераторы

        foreach ($exchanges->exchanges as $exchange) {
            $this->command->handle($exchange->exchangeId, $exchange->exchangeUrl); // команду лучше помещать в джобу и отправлять в очередь.
        }
    }
}
